import React from "react";
import { Accordion, Card, Figure, Alert } from "react-bootstrap";
import systemArchitecture from "@assets/images/System-architecture.png";
import autocomplete from "@assets/images/Autocomplete.png";

const About = () => {
  return (
    <div>
      <div className="page-title">System Design</div>
      <Accordion>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Card.Header} eventKey="0">
              <b>System Overview</b>
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <Figure>
                <Figure.Image
                  src={systemArchitecture}
                />
                <Figure.Caption>System architecture is designed on Firebase solution: Firebase Hosting (Web app), Firebase functions(API), Firebase Firestore(Database).</Figure.Caption>
              </Figure>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Card.Header} eventKey="1">
              <b>Autocomplete diagram</b>
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="1">
            <Card.Body>
              <Figure>
                <Figure.Image
                  src={autocomplete}
                />
                <Figure.Caption>Use cases of autocomplete component.</Figure.Caption>
              </Figure>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Card.Header} eventKey="3">
              <b>REST API Lists</b>
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="3">
            <Card.Body>
              <Alert key={1} variant="info">
                <Alert.Heading>{"GET /database/continent"}</Alert.Heading>
                <p><i>Return a list of all continents.</i></p>
                <hr />
                <p className="mb-0">
                  <b>Response: </b>{
                    `[
                        {
                            "code": "AF",
                            "name": "Africa",
                            "countries": [],
                            "RefDoc": "AF"
                        }
                      ]`
                  }
                </p>
              </Alert>
              <Alert key={2} variant="info">
                <Alert.Heading>{"POST /database/continent/import"}</Alert.Heading>
                <p><i>Insert a list of continents to database.</i></p>
                <hr />
                <p className="mb-0">
                  <b>Body: </b>{
                    `[
                        {
                          "code": "AF",
                          "name":"Africa"
                        }
                      ]`
                  }
                </p>
                <p><b>Response: </b>TRUE</p>
              </Alert>
              <Alert key={3} variant="primary">
                <Alert.Heading>{"GET /database/country/byContinent/:code"}</Alert.Heading>
                <p><i>Return a list of <b>first 20 countries</b> of a continent by continent code.</i></p>
                <hr />
                <p className="mb-0">
                  <b>Response: </b>{
                    `[
                        {
                            "phone_code": "213",
                            "iso2": "DZ",
                            "currency": "DZD",
                            "name": "Algeria",
                            "states": [
                                "Aïn Defla Province",
                                "Aïn Témouchent Province"
                            ],
                            "iso3": "DZA",
                            "capital": "Algiers",
                            "continent": "AF",
                            "native": "الجزائر",
                            "RefDoc": "DZA"
                        }
                      ]`
                  }
                </p>
              </Alert>
              <Alert key={4} variant="primary">
                <Alert.Heading>{"GET /database/country/nextByContinent/:code"}</Alert.Heading>
                <p><i>Return a list of <b>next 20 countries</b> of a continent by continent code.</i></p>
                <hr />
                <p className="mb-0">
                  <b>Response: </b>{
                    `[
                        {
                            "phone_code": "213",
                            "iso2": "DZ",
                            "currency": "DZD",
                            "name": "Algeria",
                            "states": [
                                "Aïn Defla Province",
                                "Aïn Témouchent Province"
                            ],
                            "iso3": "DZA",
                            "capital": "Algiers",
                            "continent": "AF",
                            "native": "الجزائر",
                            "RefDoc": "DZA"
                        }
                      ]`
                  }
                </p>
              </Alert>
              <Alert key={5} variant="primary">
                <Alert.Heading>{"POST /database/country/import"}</Alert.Heading>
                <p><i>Insert a list of countries to database.</i></p>
                <hr />
                <p className="mb-0">
                  <b>Body: </b>{
                    `[
                        {
                          "name": "Afghanistan",
                          "iso3": "AFG",
                          "iso2": "AF",
                          "phone_code": "93",
                          "capital": "Kabul",
                          "currency": "AFN",
                          "states": [
                              "Badakhshan",
                              "Badghis"
                          ],
                          "continent": "AS",
                          "native": "افغانستان"
                        }
                      ]`
                  }
                </p>
                <p><b>Response: </b>TRUE</p>
              </Alert>
              <Alert key={6} variant="success">
                <Alert.Heading>{"GET /dispatcher/searchByName/:name"}</Alert.Heading>
                <p><i>Return a list of <b>first 20 countries</b> which matches with the search string.</i></p>
                <hr />
                <p className="mb-0">
                  <b>Response: </b>{
                    `[
                        {
                            "phone_code": "213",
                            "iso2": "DZ",
                            "currency": "DZD",
                            "name": "Algeria",
                            "states": [
                                "Aïn Defla Province",
                                "Aïn Témouchent Province"
                            ],
                            "iso3": "DZA",
                            "capital": "Algiers",
                            "continent": "AF",
                            "native": "الجزائر",
                            "RefDoc": "DZA"
                        }
                      ]`
                  }
                </p>
              </Alert>
              <Alert key={7} variant="success">
                <Alert.Heading>{"GET /dispatcher/searchByNameNext/:name"}</Alert.Heading>
                <p><i>Return a list of <b>next 20 countries</b> which matches with the search string.</i></p>
                <hr />
                <p className="mb-0">
                  <b>Response: </b>{
                    `[
                        {
                            "phone_code": "213",
                            "iso2": "DZ",
                            "currency": "DZD",
                            "name": "Algeria",
                            "states": [
                                "Aïn Defla Province",
                                "Aïn Témouchent Province"
                            ],
                            "iso3": "DZA",
                            "capital": "Algiers",
                            "continent": "AF",
                            "native": "الجزائر",
                            "RefDoc": "DZA"
                        }
                      ]`
                  }
                </p>
              </Alert>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </div>
  );
};

export default About;
