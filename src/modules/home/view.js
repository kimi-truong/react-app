import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import MyAlert from "@components/MyAlert";
import Continent from "./forms/Continent";
import Autocomplete from "./forms/Autocomplete";
import { Button, Row, Col, ListGroup, Accordion, Card } from "react-bootstrap";

const Home = ({
  continents,
  countries,
  error,
  getContinents,
  getCountries,
  getNextCountries,
  searchCountries,
  searchNextCountries
}) => {
  const [selected, setSelected] = useState(null);

  useEffect(() => {
    getContinents();
  }, []);

  const loadCountries = (code, index) => {
    // expect to load 20 first items only one time for a continent
    if (continents.data[index].countries.length === 0) {
      getCountries(code, index);
    }
  };

  const loadNextCountries = (code, lastKey, index) => {
    getNextCountries(code, lastKey, index);
  };

  const autoCountries = (searchKey) => {
    searchCountries(searchKey);
  };

  const autoNextCountries = (searchKey) => {
    searchNextCountries(searchKey, countries.data[countries.data.length - 1].name);
  };

  const selectCountry = (country) => {
    setSelected(country);
  };

  return (
    <React.Fragment>
      <div className="page-title">Country Wiki</div>
      <MyAlert type="error" text={error.message} display={error.state} />
      <Row>
        <Col xs={6} md={4}>
          <Continent
            data={continents}
            handleClick={loadCountries}
            handleNext={loadNextCountries}
            handleSelect={selectCountry} />
        </Col>
        <Col xs={12} md={8}>
          <label>You can search a country to know more.</label>
          <Autocomplete
            suggestion={countries}
            handleChange={autoCountries}
            handleNext={autoNextCountries}
            handleSelect={selectCountry} />
          <div className="content">
            {selected && (
              <div>
                <div className="country-title">{selected.name}</div>
                <Accordion defaultActiveKey="0">
                  <Card>
                    <Card.Header>
                      <Accordion.Toggle as={Button} variant="link" eventKey="0">
                        About
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                      <Card.Body>
                        <Row>
                          <Col sm={4}><b>Capital</b></Col>
                          <Col sm={8}><p><i>{selected.capital}</i></p></Col>
                        </Row>
                        <Row>
                          <Col sm={4}><b>Native</b></Col>
                          <Col sm={8}><p><i>{selected.native}</i></p></Col>
                        </Row>
                        <Row>
                          <Col sm={4}><b>Currency</b></Col>
                          <Col sm={8}><p><i>{selected.currency}</i></p></Col>
                        </Row>
                        <Row>
                          <Col sm={4}><b>ISO-3 Code</b></Col>
                          <Col sm={8}><p><i>{selected.iso3}</i></p></Col>
                        </Row>
                        <Row>
                          <Col sm={4}><b>ISO-2 Code</b></Col>
                          <Col sm={8}><p><i>{selected.iso2}</i></p></Col>
                        </Row>
                        <Row>
                          <Col sm={4}><b>Phone code</b></Col>
                          <Col sm={8}><p><i>{selected.phone_code}</i></p></Col>
                        </Row>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  <Card>
                    <Card.Header>
                      <Accordion.Toggle as={Button} variant="link" eventKey="1">
                        States / Provinces ({selected.states.length})
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="1">
                      <ListGroup variant="flush">
                        {selected.states.sort((a, b) => a.localeCompare(b)).map((item, key) => (
                          <ListGroup.Item key={key}>{item}</ListGroup.Item>
                        ))}
                      </ListGroup>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>
              </div>
            )}
          </div>
        </Col>
      </Row>
    </React.Fragment>
  );
};

Home.propTypes = {
  continents: PropTypes.object,
  countries: PropTypes.object,
  error: PropTypes.object,
  getContinents: PropTypes.func,
  getCountries: PropTypes.func,
  getNextCountries: PropTypes.func,
  searchCountries: PropTypes.func,
  searchNextCountries: PropTypes.func
};

export default Home;
