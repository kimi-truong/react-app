import React, { useState } from "react";
import PropTypes from "prop-types";
import { BsX } from "react-icons/bs";
import { Button, InputGroup, FormControl } from "react-bootstrap";

const Autocomplete = ({ suggestion, handleChange, handleSelect, handleNext }) => {
  const [searchKey, setSearchKey] = useState("");
  const [expand, setExpand] = useState(false);

  const onChange = (e) => {
    handleChange(e.target.value);
    setSearchKey(e.target.value);
    setExpand(true);
  };

  const onSelect = (index, name) => (e) => {
    setSearchKey(name);
    setExpand(false);
    handleSelect(suggestion.data[index]);
  };

  const onNext = () => {
    handleNext(searchKey, suggestion.data[suggestion.data.length - 1].name);
  };

  const onClear = () => {
    setExpand(false);
    setSearchKey("");
  };
  return (
    <div className="autocomplete">
      <InputGroup>
        <FormControl
          placeholder="Enter Country Name"
          onChange={onChange}
          value={searchKey}
        />
        <InputGroup.Append>
          <Button variant="secondary" onClick={onClear}><BsX /></Button>
        </InputGroup.Append>
      </InputGroup>
      {expand && <div className="autocomplete-items">
        {suggestion.isLoading && (
          <div className="loading">loading...</div>
        )}
        {suggestion.data.map((item, index) => {
          return (
            <div key={index} onClick={onSelect(index, item.name)}>
              {item.name}
            </div>
          );
        })}
        {suggestion.isNext && <div><Button variant="link" size="sm" onClick={onNext}>More...</Button></div>}
      </div>}
    </div>
  );
};

Autocomplete.propTypes = {
  suggestion: PropTypes.object,
  handleChange: PropTypes.func,
  handleSelect: PropTypes.func,
  handleNext: PropTypes.func
};

export default Autocomplete;
