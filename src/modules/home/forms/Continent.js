import React from "react";
import PropTypes from "prop-types";
import { ListGroup, Accordion, Card, Button } from "react-bootstrap";

const Continent = ({ data, handleClick, handleNext, handleSelect }) => {
  const onClick = (code, index) => (e) => {
    return handleClick(code, index);
  };

  const onNext = (code, lastItem, index) => (e) => {
    return handleNext(code, lastItem.name, index);
  };

  const onSelect = (country) => (e) => {
    return handleSelect(country);
  };

  return (
    <Accordion className="continents">
      {data.data.map((item, key) => (
        <Card key={key}>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey={item.code} onClick={onClick(item.code, key)}>
              {item.name}
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey={item.code}>
            <Card.Body>
              <ListGroup variant="flush">
                {item.countries.map((country) => (
                  <ListGroup.Item onClick={onSelect(country)} key={country.iso3}>{country.name}</ListGroup.Item>
                ))}
                {data.isLoading && <ListGroup.Item>
                  <div className="loading">loading...</div>
                </ListGroup.Item>}
                {item.isNext && <ListGroup.Item>
                  <Button variant="link" size="sm"
                    onClick={onNext(item.code, item.countries[item.countries.length - 1], key)}>
                    More...
                  </Button>
                </ListGroup.Item>}
              </ListGroup>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      ))}
    </Accordion>
  );
};

Continent.propTypes = {
  data: PropTypes.object,
  handleClick: PropTypes.func,
  handleNext: PropTypes.func,
  handleSelect: PropTypes.func
};

export default Continent;
