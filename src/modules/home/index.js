import { connect } from "react-redux";
import Home from "./view";
import {
  GET_CONTINENTS_API,
  GET_COUNTRIES_API,
  GET_NEXT_COUNTRIES_API,
  SEARCH_COUNTRIES_API,
  SEARCH_NEXT_COUNTRIES_API
} from "./store/actions";

// Get state to props
const mapStateToProps = (state) => {
  return {
    continents: state.home.continents,
    countries: state.home.countries,
    error: state.home.error
  };
};

// Get dispatch / function to props
const mapDispatchToProps = (dispatch) => ({
  getContinents: () => dispatch({ type: GET_CONTINENTS_API }),
  getCountries: (code, index) => dispatch({ type: GET_COUNTRIES_API, payload: { code: code, index: index } }),
  getNextCountries: (code, lastKey, index) => dispatch({ type: GET_NEXT_COUNTRIES_API, payload: { code: code, lastKey: lastKey, index: index } }),
  searchCountries: (searchKey) => dispatch({ type: SEARCH_COUNTRIES_API, payload: { searchKey: searchKey } }),
  searchNextCountries: (searchKey, lastKey) => dispatch({ type: SEARCH_NEXT_COUNTRIES_API, payload: { searchKey: searchKey, lastKey: lastKey } })
});

// To make those two function works register it using connect
export default connect(mapStateToProps, mapDispatchToProps)(Home);
