import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import Home from "../view";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Home renders first time", () => {
  // const handleChange = jest.fn();
  // const handleSort = jest.fn();
  const props = {
    continents: {
      data: [],
      isLoading: false,
      limit: 20
    },
    countries: {
      data: [],
      isLoading: false,
      isNext: false,
      limit: 20
    },
    error: {},
    getContinents: jest.fn(),
    getCountries: jest.fn(),
    getNextCountries: jest.fn(),
    searchCountries: jest.fn(),
    searchNextCountries: jest.fn()
  };
  act(() => {
    render(<Home {...props} />, container);
  });
  // const total = container.querySelector(
  //   "[data-testid='total']"
  // );
  // const tableElm = container.querySelector(
  //   "[data-testid='tbl']"
  // );
  // expect(table).toEqual("");
});
