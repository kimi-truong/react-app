import { takeLatest, put, delay, call } from "redux-saga/effects";
import watchSagas, {
  watchGetContinents,
  watchGetCountries,
  watchGetNextCountries,
  watchSearchByName,
  watchSearchByNameNext
} from "../store/sagas";
import {
  getContinents,
  getCountriesByContinent,
  getNextCountriesByContinent,
  searchByName,
  searchByNameNext
} from "../store/api";
import {
  IS_AUTO_LOADING,
  IS_CONTI_LOADING,
  GET_CONTINENTS,
  GET_CONTINENTS_API,
  GET_COUNTRIES,
  GET_COUNTRIES_API,
  GET_NEXT_COUNTRIES,
  GET_NEXT_COUNTRIES_API,
  SEARCH_COUNTRIES,
  SEARCH_COUNTRIES_API,
  SEARCH_NEXT_COUNTRIES,
  SEARCH_NEXT_COUNTRIES_API,
  CLEAR_SEARCH_COUNTRIES,
  ERROR_HANDLING
} from "../store/actions";

describe("Root sagas", () => {
  /** Func watchSagas */
  describe(">>> Watch Func watchSagas", () => {
    const genObject = watchSagas();
    it("Should wait for GET_CONTINENTS_API action", () => {
      expect(genObject.next().value).toEqual(
        takeLatest(GET_CONTINENTS_API, watchGetContinents)
      );
    });

    it("Should wait for GET_COUNTRIES_API action", () => {
      expect(genObject.next().value).toEqual(
        takeLatest(GET_COUNTRIES_API, watchGetCountries)
      );
    });

    it("Should wait for GET_NEXT_COUNTRIES_API action", () => {
      expect(genObject.next().value).toEqual(
        takeLatest(GET_NEXT_COUNTRIES_API, watchGetNextCountries)
      );
    });

    it("Should wait for SEARCH_COUNTRIES_API action", () => {
      expect(genObject.next().value).toEqual(
        takeLatest(SEARCH_COUNTRIES_API, watchSearchByName)
      );
    });

    it("Should wait for SEARCH_NEXT_COUNTRIES_API action", () => {
      expect(genObject.next().value).toEqual(
        takeLatest(SEARCH_NEXT_COUNTRIES_API, watchSearchByNameNext)
      );
    });

    it("should be done on next iteration", () => {
      expect(genObject.next().done).toBeTruthy();
    });
  });

  /** Func watchSagas */
  describe(">>> Watch Func watchGetContinents", () => {
    const genObject = watchGetContinents();
    it("Should wait for getContinents API", () => {
      expect(genObject.next().value).toEqual(
        call(getContinents)
      );
    });

    it("Should wait for GET_CONTINENTS action", () => {
      expect(genObject.next([]).value).toEqual(
        put({ type: GET_CONTINENTS, payload: [] })
      );
    });

    it("should be done on next iteration", () => {
      expect(genObject.next().done).toBeTruthy();
    });
  });

  /** Func watchGetCountries */
  describe(">>> Watch Func watchGetCountries", () => {
    describe("Execute watchGetCountries successful", () => {
      const payload = {
        code: "AF",
        index: 0
      };
      const genObject = watchGetCountries({ payload });

      it("Should wait for IS_CONTI_LOADING action", () => {
        expect(genObject.next().value).toEqual(
          put({ type: IS_CONTI_LOADING })
        );
      });

      it("Should wait for getCountriesByContinent API", () => {
        expect(genObject.next().value).toEqual(
          call(getCountriesByContinent, payload.code)
        );
      });

      it("Should wait for GET_COUNTRIES action", () => {
        expect(genObject.next([]).value).toEqual(
          put({ type: GET_COUNTRIES, payload: { data: [], index: payload.index } })
        );
      });

      it("should be done on next iteration", () => {
        expect(genObject.next().done).toBeTruthy();
      });
    });

    describe("Execute watchGetCountries failed", () => {
      const payload = {};
      const genObject = watchGetCountries(payload);

      it("Should wait for IS_CONTI_LOADING action", () => {
        expect(genObject.next().value).toEqual(
          put({ type: IS_CONTI_LOADING })
        );
      });

      it("Should wait for ERROR_HANDLING action", () => {
        expect(genObject.next().value).toEqual(
          put({
            type: ERROR_HANDLING,
            payload: "Cannot read property 'code' of undefined"
          })
        );
      });

      it("should be done on next iteration", () => {
        expect(genObject.next().done).toBeTruthy();
      });
    });
  });

  /** Func watchGetNextCountries */
  describe(">>> Watch Func watchGetNextCountries", () => {
    describe("Execute watchGetNextCountries successful", () => {
      const payload = {
        code: "AF",
        lastKey: "sing",
        index: 0
      };
      const genObject = watchGetNextCountries({ payload });

      it("Should wait for IS_CONTI_LOADING action", () => {
        expect(genObject.next().value).toEqual(
          put({ type: IS_CONTI_LOADING })
        );
      });

      it("Should wait for getNextCountriesByContinent API", () => {
        expect(genObject.next().value).toEqual(
          call(getNextCountriesByContinent, payload.code, payload.lastKey)
        );
      });

      it("Should wait for GET_NEXT_COUNTRIES action", () => {
        expect(genObject.next([]).value).toEqual(
          put({ type: GET_NEXT_COUNTRIES, payload: { data: [], index: payload.index } })
        );
      });

      it("should be done on next iteration", () => {
        expect(genObject.next().done).toBeTruthy();
      });
    });

    describe("Execute watchGetNextCountries failed", () => {
      const payload = {};
      const genObject = watchGetNextCountries(payload);

      it("Should wait for IS_CONTI_LOADING action", () => {
        expect(genObject.next().value).toEqual(
          put({ type: IS_CONTI_LOADING })
        );
      });

      it("Should wait for ERROR_HANDLING action", () => {
        expect(genObject.next().value).toEqual(
          put({
            type: ERROR_HANDLING,
            payload: "Cannot read property 'code' of undefined"
          })
        );
      });

      it("should be done on next iteration", () => {
        expect(genObject.next().done).toBeTruthy();
      });
    });
  });

  /** * Func watchSearchByName ***/
  describe(">>>Watch Func watchSearchByName", () => {
    describe("Execute watchSearchByName successfully with empty search string", () => {
      const payload = {
        searchKey: ""
      };
      const genObject = watchSearchByName({ payload });
      it("Should wait for delay 200ms", () => {
        expect(genObject.next().value).toEqual(delay(200));
      });

      it("Should wait for IS_LOADING action", () => {
        expect(genObject.next().value).toEqual(
          put({ type: IS_AUTO_LOADING })
        );
      });

      it("Should wait for CLEAR_SEARCH_COUNTRIES action", () => {
        expect(genObject.next().value).toEqual(
          put({ type: CLEAR_SEARCH_COUNTRIES })
        );
      });

      it("should be done on next iteration", () => {
        expect(genObject.next().done).toBeTruthy();
      });
    });

    describe("Execute watchSearchByName successully with search string", () => {
      const payload = {
        searchKey: "sing"
      };
      const genObject = watchSearchByName({ payload });
      it("Should wait for delay 200ms", () => {
        expect(genObject.next().value).toEqual(delay(200));
      });

      it("Should wait for IS_LOADING action", () => {
        expect(genObject.next().value).toEqual(
          put({ type: IS_AUTO_LOADING })
        );
      });

      it("Should wait for searchByName API", () => {
        expect(genObject.next().value).toEqual(
          call(searchByName, payload.searchKey)
        );
      });

      it("Should wait for SEARCH_COUNTRIES action", () => {
        expect(genObject.next([]).value).toEqual(
          put({ type: SEARCH_COUNTRIES, payload: [] })
        );
      });

      it("should be done on next iteration", () => {
        expect(genObject.next().done).toBeTruthy();
      });
    });

    describe("Execute watchSearchByName failed", () => {
      const payload = {};
      const genObject = watchSearchByName(payload);
      it("Should wait for delay 200ms", () => {
        expect(genObject.next().value).toEqual(delay(200));
      });

      it("Should wait for IS_LOADING action", () => {
        expect(genObject.next().value).toEqual(
          put({ type: IS_AUTO_LOADING })
        );
      });

      it("Should wait for ERROR_HANDLING action", () => {
        expect(genObject.next().value).toEqual(
          put({
            type: ERROR_HANDLING,
            payload: "Cannot read property 'searchKey' of undefined"
          })
        );
      });

      it("should be done on next iteration", () => {
        expect(genObject.next().done).toBeTruthy();
      });
    });
  });

  /** * Func watchSearchByNameNext ***/
  describe(">>> Watch watchSearchByNameNext", () => {
    describe("Execute watchSearchByNameNext correctly", () => {
      const payload = { searchKey: "viet", lastKey: "sing" };
      const genObject = watchSearchByNameNext({ payload });

      it("Should wait for IS_LOADING action", () => {
        expect(genObject.next().value).toEqual(
          put({ type: IS_AUTO_LOADING })
        );
      });

      it("Should wait for searchByNameNext API", () => {
        expect(genObject.next().value).toEqual(
          call(searchByNameNext, payload.searchKey, payload.lastKey)
        );
      });

      it("Should wait for SEARCH_NEXT_COUNTRIES action", () => {
        expect(genObject.next([]).value).toEqual(
          put({ type: SEARCH_NEXT_COUNTRIES, payload: [] })
        );
      });

      it("should be done on next iteration", () => {
        expect(genObject.next().done).toBeTruthy();
      });
    });

    describe("Execute watchSearchByNameNext failed", () => {
      const genObject = watchSearchByNameNext({});

      it("Should wait for IS_LOADING action", () => {
        expect(genObject.next().value).toEqual(
          put({ type: IS_AUTO_LOADING })
        );
      });

      it("Should wait for ERROR_HANDLING action", () => {
        expect(genObject.next().value).toEqual(
          put({
            type: ERROR_HANDLING,
            payload: "Cannot read property 'searchKey' of undefined"
          })
        );
      });

      it("should be done on next iteration", () => {
        expect(genObject.next().done).toBeTruthy();
      });
    });
  });
});
