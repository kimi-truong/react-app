// Import the redux-saga/effects
import {
  put, call, delay,
  takeLatest
} from "redux-saga/effects";

// Import all actions
import {
  IS_AUTO_LOADING,
  IS_CONTI_LOADING,
  GET_CONTINENTS,
  GET_CONTINENTS_API,
  GET_COUNTRIES,
  GET_COUNTRIES_API,
  GET_NEXT_COUNTRIES,
  GET_NEXT_COUNTRIES_API,
  SEARCH_COUNTRIES,
  SEARCH_COUNTRIES_API,
  SEARCH_NEXT_COUNTRIES,
  SEARCH_NEXT_COUNTRIES_API,
  CLEAR_SEARCH_COUNTRIES,
  ERROR_HANDLING
} from "./actions";

// Import all api's
import {
  getContinents,
  getCountriesByContinent,
  getNextCountriesByContinent,
  searchByName,
  searchByNameNext
} from "./api";

/**
 * Saga to listen GET_CONTINENTS_API action
 * Get a list of continents and
 * Dispatch to action GET_CONTINENTS
 */
export function* watchGetContinents() {
  try {
    const data = yield call(getContinents);
    yield put({ type: GET_CONTINENTS, payload: data });
  } catch (err) {
    yield put({
      type: ERROR_HANDLING,
      payload: err.message
    });
  }
}

/**
 * Saga to listen GET_COUNTRIES_API action
 * Get a list of first 20 contries for a continent and
 * Dispatch to action GET_COUNTRIES
 */
export function* watchGetCountries({ payload }) {
  try {
    yield put({ type: IS_CONTI_LOADING });
    const data = yield call(getCountriesByContinent, payload.code);
    yield put({ type: GET_COUNTRIES, payload: { data: data, index: payload.index } });
  } catch (err) {
    yield put({
      type: ERROR_HANDLING,
      payload: err.message
    });
  }
}

/**
 * Saga to listen GET_NEXT_COUNTRIES_API action
 * Get a list of next 20 contries for a continent and
 * Dispatch to action GET_NEXT_COUNTRIES
 */
export function* watchGetNextCountries({ payload }) {
  try {
    yield put({ type: IS_CONTI_LOADING });
    const data = yield call(getNextCountriesByContinent, payload.code, payload.lastKey);
    yield put({ type: GET_NEXT_COUNTRIES, payload: { data: data, index: payload.index } });
  } catch (err) {
    yield put({
      type: ERROR_HANDLING,
      payload: err.message
    });
  }
}

/**
 * Saga to listen SEARCH_COUNTRIES_API action
 * Get a list of first 20 countries and
 * Dispatch to action SEARCH_COUNTRIES
 */
export function* watchSearchByName({ payload }) {
  try {
    yield delay(200);
    yield put({ type: IS_AUTO_LOADING });
    if (payload.searchKey === "") yield put({ type: CLEAR_SEARCH_COUNTRIES });
    else {
      const data = yield call(searchByName, payload.searchKey);
      yield put({ type: SEARCH_COUNTRIES, payload: data });
    }
  } catch (err) {
    yield put({
      type: ERROR_HANDLING,
      payload: err.message
    });
  }
}

/**
 * Saga to listen SEARCH_NEXT_COUNTRIES_API action
 * Get a list of next 20 countries and
 * Dispatch to action SEARCH_NEXT_COUNTRIES
 */
export function* watchSearchByNameNext({ payload }) {
  try {
    yield put({ type: IS_AUTO_LOADING });
    const data = yield call(searchByNameNext, payload.searchKey, payload.lastKey);
    yield put({ type: SEARCH_NEXT_COUNTRIES, payload: data });
  } catch (err) {
    yield put({
      type: ERROR_HANDLING,
      payload: err.message
    });
  }
}

export default function* watchSagas() {
  yield takeLatest(GET_CONTINENTS_API, watchGetContinents);
  yield takeLatest(GET_COUNTRIES_API, watchGetCountries);
  yield takeLatest(GET_NEXT_COUNTRIES_API, watchGetNextCountries);
  yield takeLatest(SEARCH_COUNTRIES_API, watchSearchByName);
  yield takeLatest(SEARCH_NEXT_COUNTRIES_API, watchSearchByNameNext);
}
