import defaultAxios from "axios";

const urls = {
  dev: "http://localhost:5001/kimi-assessment/us-central1/",
  prod: "https://us-central1-kimi-assessment.cloudfunctions.net/"
};
const axios = defaultAxios.create({
  baseURL: urls[process.env.NODE_ENV] || urls.prod,
  headers: {
    "Content-Type": "application/json"
  }
});

/**
 * Returns a list of continents.
 * @return {array}
 */
export const getContinents = async () => {
  try {
    const res = await axios.get("database/continent");
    return res.data;
  } catch (err) {
    throw new Error("Cannot execute database/continent API", err.message);
  }
};

/**
* Returns a list of first 20 countries of a continents.
* @param {string} code Continent Code
* @return {array}
*/
export const getCountriesByContinent = async (code) => {
  try {
    const res = await axios.get(`database/country/byContinent/${code}`);
    return res.data;
  } catch (err) {
    throw new Error("Cannot execute database/country/byContinent API", err.message);
  }
};

/**
* Returns a list of next 20 countries of a continents.
* @param {string} code Continent Code
* @param {string} lastKey next from the last item
* @return {array}
*/
export const getNextCountriesByContinent = async (code, lastKey) => {
  try {
    const res = await axios.get(`database/country/nextByContinent/${code}/${lastKey}`);
    return res.data;
  } catch (err) {
    throw new Error("Cannot execute database/country/nextByContinent API", err.message);
  }
};

/**
 * Returns a list of first 20 countries.
 * @param {string} searchKey
 * @return {array}
 */
export const searchByName = async (searchKey) => {
  try {
    const res = await axios.get(`dispatcher/searchByName/${searchKey}`);
    return res.data;
  } catch (err) {
    throw new Error("Cannot execute dispatcher/searchByName API", err.message);
  }
};

/**
 * Returns a list of next 20 countries.
 * @param {string} searchKey
 * @param {string} lastKey next from the last item
 * @return {array}
 */
export const searchByNameNext = async (searchKey, lastKey) => {
  try {
    const res = await axios.get(`dispatcher/searchByNameNext/${searchKey}/${lastKey}`);
    return res.data;
  } catch (err) {
    throw new Error("Cannot execute dispatcher/searchByNameNext API", err.message);
  }
};
