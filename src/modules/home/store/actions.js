export const IS_AUTO_LOADING = "IS_AUTO_LOADING";
export const IS_CONTI_LOADING = "IS_CONTI_LOADING";
export const GET_CONTINENTS = "GET_CONTINENTS";
export const GET_CONTINENTS_API = "GET_CONTINENTS_API";
export const GET_COUNTRIES = "GET_COUNTRIES";
export const GET_COUNTRIES_API = "GET_COUNTRIES_API";
export const GET_NEXT_COUNTRIES = "GET_NEXT_COUNTRIES";
export const GET_NEXT_COUNTRIES_API = "GET_NEXT_COUNTRIES_API";
export const SEARCH_COUNTRIES = "SEARCH_COUNTRIES";
export const SEARCH_COUNTRIES_API = "SEARCH_COUNTRIES_API";
export const SEARCH_NEXT_COUNTRIES = "SEARCH_NEXT_COUNTRIES";
export const SEARCH_NEXT_COUNTRIES_API = "SEARCH_NEXT_COUNTRIES_API";
export const CLEAR_SEARCH_COUNTRIES = "CLEAR_SEARCH_COUNTRIES";
export const ERROR_HANDLING = "ERROR_HANDLING";
