import {
  IS_AUTO_LOADING,
  IS_CONTI_LOADING,
  GET_CONTINENTS,
  GET_COUNTRIES,
  GET_NEXT_COUNTRIES,
  SEARCH_COUNTRIES,
  SEARCH_NEXT_COUNTRIES,
  CLEAR_SEARCH_COUNTRIES,
  ERROR_HANDLING
} from "./actions";

const initialState = {
  continents: {
    data: [],
    isLoading: false,
    limit: 20
  },
  countries: {
    data: [],
    isLoading: false,
    isNext: false,
    limit: 20
  },
  error: {
    state: false,
    message: ""
  }
};

const reducer = (state = initialState, { type, payload }) => {
  try {
    switch (type) {
      case IS_AUTO_LOADING:
        return {
          ...state,
          countries: {
            ...state.countries,
            isLoading: true,
            isNext: false
          }
        };
      case IS_CONTI_LOADING:
        return {
          ...state,
          continents: {
            ...state.continents,
            isLoading: true
          }
        };
      case GET_CONTINENTS:
        return {
          ...state,
          continents: {
            ...state.continents,
            data: payload,
            isLoading: false
          }
        };
      case GET_COUNTRIES: {
        const data = [...state.continents.data];
        data[payload.index] = {
          ...state.continents.data[payload.index],
          countries: payload.data,
          isNext: !(payload.data.length < state.continents.limit)
        };
        return {
          ...state,
          continents: {
            ...state.continents,
            data: data,
            isLoading: false
          }
        };
      };
      case GET_NEXT_COUNTRIES: {
        const countries = [...state.continents.data[payload.index].countries];
        const data = [...state.continents.data];
        data[payload.index] = {
          ...state.continents.data[payload.index],
          countries: [...countries, ...payload.data],
          isNext: !(payload.data.length < state.continents.limit)
        };
        return {
          ...state,
          continents: {
            ...state.continents,
            data: data,
            isLoading: false
          }
        };
      };
      case SEARCH_COUNTRIES:
        return {
          ...state,
          countries: {
            ...state.countries,
            data: payload,
            isLoading: false,
            isNext: !(payload.length < state.countries.limit)
          }
        };
      case SEARCH_NEXT_COUNTRIES:
        return {
          ...state,
          countries: {
            ...state.countries,
            data: [...state.countries.data, ...payload],
            isLoading: false,
            isNext: !(payload.length < state.countries.limit)
          }
        };
      case CLEAR_SEARCH_COUNTRIES:
        return {
          ...state,
          countries: {
            ...state.countries,
            data: [],
            isLoading: false,
            isNext: false
          }
        };
      case ERROR_HANDLING:
        return {
          ...state,
          countries: {
            ...state.countries,
            data: [],
            isLoading: false,
            isNext: false
          },
          error: {
            state: true,
            message: payload
          }
        };
      default:
        return state;
    }
  } catch (err) {
    return {
      countries: [],
      error: {
        state: true,
        message: err.message
      }
    };
  }
};

export default reducer;
