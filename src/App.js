import "@assets/scss/index.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { Provider } from "react-redux";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useLocation
} from "react-router-dom";
import Home from "./modules/home";
import About from "./modules/about";
import store from "./stores";

const Menu = () => {
  const location = useLocation();
  const { pathname } = location;
  return (
    <ul className="menu">
      <li>
        <Link to="/" className={pathname === "/" ? "active" : ""}>Demo</Link>
      </li>
      <li>
        <Link to="/about" className={pathname === "/about" ? "active" : ""}>About System</Link>
      </li>
    </ul>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <div>
          <header>
            <p>Coding Assessment</p>
            <nav>
              <Menu />
            </nav>
          </header>

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <main>
            <Switch>
              <Route path="/about">
                <About />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </main>
          <footer>
            <p>Copyright © 2021 Kimi Truong.</p>
          </footer>
        </div>
      </Router>
    </Provider>
  );
};

export default App;
