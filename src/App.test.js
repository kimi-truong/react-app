import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("it should render home page", () => {
  render(<App />);
  const linkElement = screen.getByText(/Coding Assessment/i);
  expect(linkElement).toBeInTheDocument();
});
