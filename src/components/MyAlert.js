import React from "react";
import PropTypes from "prop-types";
import { Alert } from "react-bootstrap";

const MyAlert = ({ type, text, display }) => {
  return (
    <Alert show={display} variant={type} data-testid="alert">
      {text}
    </Alert>
  );
};

MyAlert.propTypes = {
  text: PropTypes.string,
  type: PropTypes.string,
  display: PropTypes.bool
};

export default MyAlert;
