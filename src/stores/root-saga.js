import { spawn } from "redux-saga/effects";

// Sagas
import home from "@home/store/sagas";

// Export the root saga
export default function* rootSaga() {
  console.log("Hello From Redux-Saga!");
  yield spawn(home);
}
