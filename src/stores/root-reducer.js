// combineReducers come from redux that used for combining reducers that we just made.
import { combineReducers } from "redux";

// Reducers
import home from "@home/store/reducers";

export default combineReducers({
  home
  // Here you can registering another reducers.
});
