/**
  @desc createStore is used for creating a store for our redux
  @desc applyMiddleware is used for applying some middleware to redux, in this case we gonna using redux-saga
*/
import { createStore, applyMiddleware } from "redux";

// composeWithDevTools is tools that gonna be connecting our application for debugging the redux into the browser
import { composeWithDevTools } from "redux-devtools-extension";

// This is the middleware that we gonna use redux-saga
import createSagaMiddleware from "redux-saga";

// Root Reducers
import reducers from "./root-reducer";

// Root Sagas
import sagas from "./root-saga";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);

// Run redux-saga
sagaMiddleware.run(sagas);
export default store;
