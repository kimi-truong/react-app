# CODING ASSESSMENT

### `npm install -g firebase-tools`
Must be installed to run firebase cli in this project.

## Back-end

In the functions directory, you can run:

### `yarn serve`
Runs the functions in the development mode: http://localhost:5001/kimi-assessment/us-central1/


### `yarn deploy`
Deploy all functions to firebase project: https://us-central1-kimi-assessment.cloudfunctions.net/



## Front-end

In the project directory, you can run:

### `yarn start`
Runs web app in the development mode: http://localhost:3000


### `yarn test`
Runs Unit test


### `yarn build`
Builds web app to build folder

### `yarn eslint`
Runs style in project.


### `yarn deploy`
Deploys web app to firebase hosting: https://kimi-assessment.web.app

## CI/CD
Uses bitbucket pipelines, check file: bitbucket-pipelines.yml





