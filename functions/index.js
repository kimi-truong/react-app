const functions = require("firebase-functions")
const database = require("./modules/database/app")
const searching = require("./modules/searching/app")
const dispatcher = require("./modules/dispatcher/app")

exports.database = functions.https.onRequest(database)
exports.searching1 = functions.https.onRequest(searching)
exports.searching2 = functions.https.onRequest(searching)
exports.dispatcher = functions.https.onRequest(dispatcher)
