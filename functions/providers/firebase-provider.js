"user strict"

const admin = require("firebase-admin")
const config = require("../config/firebase-config.json")

admin.initializeApp({
  credential: admin.credential.cert(config.credential),
  databaseURL: config.databaseURL
})

const db = admin.firestore()
db.settings({ timestampsInSnapshots: true })

module.exports = {
  admin: admin,
  db: db
}
