const express = require("express")
const axios = require("axios")
const cors = require("cors")
const rateLimit = require("express-rate-limit")
const functions = require("firebase-functions")
const config = require("../../config/dispatch-config.json")

const app = express()
let cur = 0
const env = functions.config().service ? functions.config().service.env : "dev"
const servers = config[env].servers

const apiLimiter = rateLimit({
  windowMs: 1000,
  max: 30,
  handler: (req, res, next) => {
    cur = (cur + 1) % servers.length
    next()
  }
})

const dispatch = async (req, res) => {
  console.log("Dispatch to", servers[cur])
  const _req = await axios({
    url: servers[cur] + req.url,
    responseType: "stream"
  }).catch((err) => {
    res.status(500).send("Error connecting to the microservice: ", err.message)
  })
  _req.data.pipe(res)
}
app.use(cors({ origin: true }))
app.all("*", apiLimiter, dispatch)

module.exports = app
