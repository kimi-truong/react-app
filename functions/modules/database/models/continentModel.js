"user strict"
const db = require("../../../providers/firebase-provider").db
const continents = db.collection("continents")

module.exports = {
  get: () => {
    return new Promise((resolve, reject) => {
      continents
        .orderBy("name")
        .get()
        .then(snapshot => {
          const lst = snapshot.docs.map(item => {
            let obj = {}
            obj = item.data()
            obj.countries = []
            obj.RefDoc = item.id
            return obj
          })
          resolve(lst)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  /**
  * Insert a list of continent to DB.
  * @param {array} data
  * @return {bool}
  */
  import: data => {
    return new Promise((resolve, reject) => {
      const batch = db.batch()
      data.forEach(item => {
        const ref = continents.doc(item.code)
        batch.set(ref, item)
      })
      // Commit the batch
      batch
        .commit()
        .then(res => {
          resolve(true)
        })
        .catch(err => {
          reject(err)
        })
    })
  }
}
