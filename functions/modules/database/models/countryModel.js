"user strict"
const db = require("../../../providers/firebase-provider").db
const countries = db.collection("countries")

module.exports = {
  get: () => {
    return new Promise((resolve, reject) => {
      countries
        .orderBy("name")
        .limit(20)
        .get()
        .then(snapshot => {
          const lst = snapshot.docs.map(item => {
            let obj = {}
            obj = item.data()
            obj.RefDoc = item.id
            return obj
          })
          resolve(lst)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  /**
  * Returns a list of next 20 countries.
  * @param {string} lastKey next from lastKey
  * @return {array}
  */
  getNext: (lastKey) => {
    return new Promise((resolve, reject) => {
      countries
        .orderBy("name")
        .startAfter(lastKey)
        .limit(20)
        .get()
        .then(snapshot => {
          const lst = snapshot.docs.map(item => {
            let obj = {}
            obj = item.data()
            obj.RefDoc = item.id
            return obj
          })
          resolve(lst)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  /**
  * Returns a list of first 20 countries of a continents.
  * @param {string} code Continent Code
  * @return {array}
  */
  getByContinent: (code) => {
    return new Promise((resolve, reject) => {
      countries
        .where("continent", "==", code)
        .orderBy("name")
        .limit(20)
        .get()
        .then(snapshot => {
          const lst = snapshot.docs.map(item => {
            let obj = {}
            obj = item.data()
            obj.RefDoc = item.id
            return obj
          })
          resolve(lst)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  /**
  * Returns a list of next 20 countries.
  * @param {string} code Continent Code
  * @param {string} lastKey Next from lastKey
  * @return {array}
  */
  getNextByContinent: (code, lastKey) => {
    return new Promise((resolve, reject) => {
      countries
        .where("continent", "==", code)
        .orderBy("name")
        .startAfter(lastKey)
        .limit(20)
        .get()
        .then(snapshot => {
          const lst = snapshot.docs.map(item => {
            let obj = {}
            obj = item.data()
            obj.RefDoc = item.id
            return obj
          })
          resolve(lst)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  /**
  * Insert a list of country to DB.
  * @param {array} data
  * @return {bool}
  */
  import: data => {
    return new Promise((resolve, reject) => {
      const batch = db.batch()
      data.forEach(item => {
        const ref = countries.doc(item.iso3)
        batch.set(ref, item)
      })
      // Commit the batch
      batch
        .commit()
        .then(res => {
          resolve(true)
        })
        .catch(err => {
          reject(err)
        })
    })
  }
}
