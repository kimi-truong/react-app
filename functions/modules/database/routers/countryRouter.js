const express = require("express")
const router = express.Router()
const controller = require("../controllers/countryController")

router.get("/", controller.get)
router.get("/next/:lastKey", controller.getNext)
router.get("/byContinent/:code", controller.getByContinent)
router.get("/nextByContinent/:code/:lastKey", controller.getNextByContinent)
router.post("/import", controller.import)

module.exports = router
