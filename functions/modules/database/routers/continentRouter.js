const express = require("express")
const router = express.Router()
const controller = require("../controllers/continentController")

router.get("/", controller.get)
router.post("/import", controller.import)

module.exports = router
