"use strict"
const model = require("../models/continentModel")
const { validateContinent } = require("./validator")

module.exports = {
  get: async (req, res, next) => {
    try {
      const res = await model.get()
      next(res)
    } catch (err) {
      next(err)
    }
  },
  import: async (req, res, next) => {
    try {
      const data = req.body
      let res = []
      if (validateContinent(data)) {
        res = await model.import(data)
      }
      next(res)
    } catch (err) {
      next(err)
    }
  }
}
