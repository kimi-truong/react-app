"use strict"
const model = require("../models/countryModel")
const { validateCountry, createKeywords } = require("./validator")

module.exports = {
  get: async (req, res, next) => {
    try {
      const res = await model.get()
      next(res)
    } catch (err) {
      next(err)
    }
  },
  getNext: async (req, res, next) => {
    try {
      const { lastKey } = req.params
      const res = await model.getNext(lastKey)
      next(res)
    } catch (err) {
      next(err)
    }
  },
  getByContinent: async (req, res, next) => {
    try {
      const { code } = req.params
      const res = await model.getByContinent(code)
      next(res)
    } catch (err) {
      next(err)
    }
  },
  getNextByContinent: async (req, res, next) => {
    try {
      const { code, lastKey } = req.params
      const res = await model.getNextByContinent(code, lastKey)
      next(res)
    } catch (err) {
      next(err)
    }
  },
  import: async (req, res, next) => {
    try {
      const data = req.body
      let res = []
      if (validateCountry(data)) {
        const lst = createKeywords(data)
        res = await model.import(lst)
      }
      next(res)
    } catch (err) {
      next(err)
    }
  }
}
