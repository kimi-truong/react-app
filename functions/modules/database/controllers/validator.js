"user strict"

module.exports = {
  validateCountry: (data) => {
    if (data.length > 500) {
      throw new Error("Data size is too large, 500 docs only.")
    }
    const error = []
    data.forEach((item, key) => {
      if (item.name === "" ||
        item.iso3 === "" ||
        item.iso2 === "" ||
        item.continent === "" ||
        item.states.length === 0
      ) { error.push(key) }
    })
    if (error.length === 0) return true
    else {
      throw new Error(`Invalid Item(s) at index: ${error.toString()}`)
    }
  },

  validateContinent: (data) => {
    if (data.length > 500) {
      throw new Error("Data size is too large, 500 docs only.")
    }
    const error = []
    data.forEach((item, key) => {
      if (item.name === "" ||
        item.code === ""
      ) { error.push(key) }
    })
    if (error.length === 0) return true
    else {
      throw new Error(`Invalid Item(s) at index: ${error.toString()}`)
    }
  },

  /**
  * Returns a sorted array by key and type (asc or desc)
  * @param {array} data original array.
  * @return {array} sorted array
  */
  createKeywords: (data) => {
    return data.map(item => {
      const arrName = []
      let cur = "";
      [...item.name].forEach(s => {
        cur += s
        arrName.push(cur.toLowerCase())
      })
      return { ...item, ...{ nameKeys: arrName } }
    })
  }
}
