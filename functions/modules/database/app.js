const createError = require("http-errors")
const express = require("express")
const logger = require("morgan")
const cors = require("cors")
const continentRouter = require("./routers/continentRouter")
const countryRouter = require("./routers/countryRouter")

const app = express()
app.use(cors({ origin: true }))
app.use(logger("dev"))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use("/continent", continentRouter)
app.use("/country", countryRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// response handler
app.use(function (data, req, res, next) {
  if (data.message) { // if error
    console.error(data)
    res.status(data.status || 500)
    res.json(data.message)
  } else {
    res.json(data)
  }
})

module.exports = app
