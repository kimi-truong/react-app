"user strict"
const db = require("../../providers/firebase-provider").db
const countries = db.collection("countries")

module.exports = {
  searchByName: (value) => {
    return new Promise((resolve, reject) => {
      countries
        .where("nameKeys", "array-contains", value.toLowerCase())
        .orderBy("name")
        .limit(20)
        .get()
        .then(snapshot => {
          const lst = snapshot.docs.map(item => {
            let obj = {}
            obj = item.data()
            obj.RefDoc = item.id
            return obj
          })
          resolve(lst)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  searchByNameNext: (value, lastKey) => {
    return new Promise((resolve, reject) => {
      countries
        .where("nameKeys", "array-contains", value.toLowerCase())
        .orderBy("name")
        .startAfter(lastKey)
        .limit(20)
        .get()
        .then(snapshot => {
          const lst = snapshot.docs.map(item => {
            let obj = {}
            obj = item.data()
            obj.RefDoc = item.id
            return obj
          })
          resolve(lst)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}
