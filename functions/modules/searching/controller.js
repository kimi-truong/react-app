"use strict"
const model = require("./model.js")

module.exports = {
  searchByName: async (req, res, next) => {
    try {
      const { name } = req.params
      if (name && name !== "") {
        const res = await model.searchByName(name)
        next(res)
      } else {
        throw new Error("Search string is invalid")
      }
    } catch (err) {
      next(err)
    }
  },
  searchByNameNext: async (req, res, next) => {
    try {
      const { name, lastKey } = req.params
      if (name && lastKey && name !== "" && lastKey !== "") {
        const res = await model.searchByNameNext(name, lastKey)
        next(res)
      } else {
        throw new Error("Search string or Last Key is invalid")
      }
    } catch (err) {
      next(err)
    }
  }
}
