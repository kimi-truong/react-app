const express = require("express")
const router = express.Router()
const controller = require("./controller.js")

router.get("/searchByName/:name", controller.searchByName)
router.get("/searchByNameNext/:name/:lastKey", controller.searchByNameNext)

module.exports = router
