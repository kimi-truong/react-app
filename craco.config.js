const path = require("path");
module.exports = {
  webpack: {
    alias: {
      "@assets": path.resolve(__dirname, "src/assets/"),
      "@components": path.resolve(__dirname, "src/components/"),
      "@home": path.resolve(__dirname, "src/modules/home/")
    }
  },
  jest: {
    configure: {
      moduleNameMapper: {
        "^@assets/(.*)": "<rootDir>/src/assets/$1",
        "^@components/(.*)": "<rootDir>/src/components/$1",
        "^@home/(.*)": "<rootDir>/src/modules/home/$1"

      }
    }

  }
};
